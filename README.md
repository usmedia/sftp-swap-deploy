# Bitbucket Pipelines Pipe: SFTP Swap deploy

Deploy a folder to a remote server using SFTP and swap it an existing folder.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:
    
```yaml
- pipe: usmedia/sftp-swap-deploy:0.1.0
  variables:
    USER: '<string>'
    SERVER: '<string>'
    REMOTE_PATH: '<string>'
    REMOTE_FOLDER: '<string>'
    LOCAL_PATH: '<string>'
    # SSH_KEY: '<string>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable              | Usage                 |
| --------------------- | --------------------- |
| SERVER (*)            | The remote host to transfer the files to. |
| USER (*)              | The user on the remote host to connect as. |
| REMOTE_PATH (*)       | The remote path to deploy files to. |
| REMOTE_FOLDER (*)     | The name of the folder to swap new files into. Is appended to REMOTE_PATH. |
| LOCAL_PATH (*)        | The local path to folder containing files to be deployed. LOCAL_PATH may contain glob characters and may match multiple files. |
| SSH_KEY               | An alternate SSH_KEY to use instead of the key configured in the Bitbucket Pipelines admin screens (which is used by default). This should be encoded as per the instructions given in the docs for [using multiple ssh keys](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html#UseSSHkeysinBitbucketPipelines-multiple_keys) |
| EXTRA_ARGS            | Additional arguments passed to the sftp command (see [SFTP docs](http://manpages.ubuntu.com/manpages/trusty/en/man1/sftp.1.html) for more details). |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Details

SFTP copies files and directories from your local system to another server. It uses SSL for data transfer, which uses the same authentication
and provides the same security as SSH. It may also use many other features of SSH, such as compression.

More details: [sftp manual page](http://manpages.ubuntu.com/manpages/trusty/en/man1/sftp.1.html)

By default, the SFTP pipe will automatically use your configured SSH key and known_hosts file configured from the [Bitbucket Pipelines
administration pages](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html). You can pass
the `SSH_KEY` parameter to use an alternate SSH key as per the instructions in the docs for
[using multiple ssh keys](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html#UseSSHkeysinBitbucketPipelines-multiple_keys)


## Prerequisites
* If you want to use the default behaviour for using the configured SSH key and known hosts file, you must have configured 
  the SSH private key and known_hosts to be used for the SFTP pipe in your Pipelines settings
  (see [docs](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html))
 
## Examples

### Basic example:
    
```yaml
script:
  - pipe: usmedia/sftp-swap-deploy:0.1.0
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/html/'
      REMOTE_FOLDER: 'static'
      LOCAL_PATH: 'dist'
```

### Advanced examples:
Here we pass extra arguments to the sftp command and enable extra debugging and use port 22324.
    
```yaml
script:
  - pipe: usmedia/sftp-swap-deploy:0.1.0
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/html/'
      REMOTE_FOLDER: 'static'
      LOCAL_PATH: 'dist'
      DEBUG: 'true'
      EXTRA_ARGS: '-P 22324'

```

Example with alternate SSH key.
    
```yaml
script:
  - pipe: usmedia/sftp-swap-deploy:0.1.0
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/html/'
      REMOTE_FOLDER: 'static'
      LOCAL_PATH: 'dist'
      SSH_KEY: $MY_SSH_KEY
      DEBUG: 'true'
      EXTRA_ARGS: '-o ServerAliveInterval=10'
```

## License
Copyright (c) 2019 Us Media B.V.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
