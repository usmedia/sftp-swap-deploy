FROM alpine:3.8

RUN apk add --update --no-cache openssh sshpass bash

COPY pipe.sh /usr/bin/
ENTRYPOINT ["pipe.sh"]
